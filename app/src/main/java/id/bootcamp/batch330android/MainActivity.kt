package id.bootcamp.batch330android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import id.bootcamp.batch330android.activitystack.Stack1Activity
import id.bootcamp.batch330android.activitystack.Stack2Activity
import id.bootcamp.batch330android.registration.RegistrationFormActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        //set layout
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //ambil object button
        val btnStackActivity = findViewById<Button>(R.id.btnActivityStuck)
        val buttonRegister = findViewById<Button>(R.id.buttonRegister)

        //atur button setelah di klik
        btnStackActivity.setOnClickListener {
            //buat objek intent
            //untuk memberitahukan navigasi dari apa ke apa
            val intent = Intent(this, Stack1Activity::class.java)
            startActivity(intent)
        }

        buttonRegister.setOnClickListener {
            //kalo misal button register
            //jalankan perintah didalam sini
            val intent = Intent(this, RegistrationFormActivity::class.java)
            startActivity(intent)
        }



        Log.d("activity_lifecycle", "OnCreate Terpanggil")
    }
}


//            //perintah munculin toast
//            Toast.makeText(this,"Button Stack Activity diKlik",
//                Toast.LENGTH_SHORT).show()
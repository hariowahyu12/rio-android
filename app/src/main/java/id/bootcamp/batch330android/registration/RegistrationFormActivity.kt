package id.bootcamp.batch330android.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.Toast
import id.bootcamp.batch330android.R
import id.bootcamp.batch330android.utils.isValidEmail

class RegistrationFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //printah memasang layout
        setContentView(R.layout.activity_registration_form)

        //ambil semua objek yang ada di form
        val editTextFirstName = findViewById<EditText>(R.id.etFirstName)
        val editTextLastName = findViewById<EditText>(R.id.etLastName)

        val RadioButtonMale = findViewById<RadioButton>(R.id.radioButtonMale)
        val RadioButtonFemale = findViewById<RadioButton>(R.id.radioButtonFemale)

        val cbInggris = findViewById<CheckBox>(R.id.cbInggris)
        val cbIndonesia = findViewById<CheckBox>(R.id.cbIndonesia)

        val editTextEmail = findViewById<EditText>(R.id.etEmail)

        val editTextAddress = findViewById<EditText>(R.id.etAddress)

        val spinner = findViewById<Spinner>(R.id.spinner)

        val buttonSubmit = findViewById<Button>(R.id.buttonSubmit)

        //ambil array string dari resource
        val states = resources.getStringArray(R.array.states)

        //mengisi spinner/dropwoen
        val adapter = ArrayAdapter(this,
        android.R.layout.simple_list_item_1,
            states)
        spinner.adapter = adapter

        //buat clicklistener buttonsubmit

        buttonSubmit.setOnClickListener {
            //ambil value dari form
            //edit text
            val firstname = editTextFirstName.text.toString()
            val lastname = editTextLastName.text.toString()
            val email = editTextEmail.text.toString()
            val address = editTextAddress.text.toString()

            //radio button
            var gender = ""
            if (RadioButtonMale.isChecked){
                gender = "Male"
            }else if(RadioButtonFemale.isChecked){
                gender = "Female"
            }

            //checkbox
            var listLanguage = ArrayList<String>()
            if (cbIndonesia.isChecked){
                //perintah buat menambahkan data di list language
                listLanguage.add("Indonesia")
            }
            if(cbInggris.isChecked){
                listLanguage.add("Inggris")
            }


            //spinner
            val state = spinner.selectedItem.toString()

            //logic validation
            //first name tdk boleh kosong
            if(firstname.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                    "First Name tidak boleh kosong",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //last name tdk boleh kosong
            if(lastname.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Last Name tidak boleh kosong",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //gender harus dipilih
            if(gender.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Gender harus dipilih",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }
            //email harus valid
            val isEmailValid = isValidEmail(email)
            if (isEmailValid == false){
                Toast.makeText(this@RegistrationFormActivity,
                    "Email Tidak Valid",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //address tidak boleh kosong
            if(address.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Address tidak boleh kosong",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //language harus dipilih minimal 1
            val intent = Intent(this,RegistrationResultActivity::class.java )
            intent.putExtra("keyFirstName", firstname)
            intent.putExtra("keyLastName", lastname)
            intent.putExtra("keyEmail", email)
            intent.putExtra("keyAddress", address)
            intent.putExtra("keyGender", gender)
            intent.putExtra("keyLanguage", listLanguage)
            intent.putExtra("keyState", state)
            startActivity(intent)
        }
    }
}
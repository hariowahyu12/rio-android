package id.bootcamp.batch330android.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import id.bootcamp.batch330android.R

class RegistrationResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_result)

        //ambil data yang dikirim dari activity sebelumnya

        val firstName = intent.extras?.getString("keyFirstName")
        val lastName = intent.extras?.getString("keyLastName")
        val email = intent.extras?.getString("keyEmail")
        val address = intent.extras?.getString("keyAddress")
        val gender = intent.extras?.getString("keyGender")
        val language = intent.extras?.getStringArrayList("keyLanguage")
        val state = intent.extras?.getString("keyState")

        //ambil objek dari layout
        val textFirstName = findViewById<TextView>(R.id.textFirstName)
        val textLastName = findViewById<TextView>(R.id.textLastName)
        val textGender = findViewById<TextView>(R.id.textGender)
        val textEmail = findViewById<TextView>(R.id.textEmail)
        val textAddress = findViewById<TextView>(R.id.textAddress)
        val textState = findViewById<TextView>(R.id.textState)
        val textLanguage = findViewById<TextView>(R.id.textLanguage)

        //set text data dari activity sebelumnya
        textFirstName.text = "First Name: $firstName"
        textLastName.text = "Last Name: $lastName"
        textGender.text = "Gender: $gender"
        textEmail.text = "Email: $email"
        textAddress.text = "Address: $address"
        textState.text = "State: $state"
        textLanguage.text = "language: $language"
    }
}
package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import id.bootcamp.batch330android.MainActivity
import id.bootcamp.batch330android.R

class Stack2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack2)

        //ambil object button
        val btnGoMainMenu = findViewById<Button>(R.id.btnGoMainMenu)
        //atur button setelah di klik
        btnGoMainMenu.setOnClickListener {
//            //perintah munculin toast
//            Toast.makeText(this,"Button Stack Activity diKlik",
//                Toast.LENGTH_SHORT).show()
//        }

            //buat objek intent
            //untuk memberitahukan navigasi dari apa ke apa
            val intent = Intent(this, MainActivity::class.java)
            //fungsi tambahan untuk clear semua stack sebelum pindah ke MainActivity
            intent.flags=Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)

            Log.d("activity_lifecycle", "OnCreate Terpanggil")
        }
    }
}
package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import id.bootcamp.batch330android.R

class Stack1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack1)

        //ambil object button
        val btnGoStack2 = findViewById<Button>(R.id.btnGoStack2)
        //atur button setelah di klik
        btnGoStack2.setOnClickListener {
//            //perintah munculin toast
//            Toast.makeText(this,"Button Stack Activity diKlik",
//                Toast.LENGTH_SHORT).show()
//        }

            //buat objek intent
            //untuk memberitahukan navigasi dari apa ke apa
            val intent = Intent(this, Stack2Activity::class.java)
            startActivity(intent)

            Log.d("activity_lifecycle", "OnCreate Terpanggil")
        }
    }
}

